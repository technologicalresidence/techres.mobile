import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ItemSelect, Resident, Visit } from '../../models/index';
import { ToastService, VisitsService } from '../../services/index';

@Component({
  selector: 'app-create-visit',
  templateUrl: './create-visit.component.html',
  styleUrls: ['./create-visit.component.scss'],
})
export class CreateVisitComponent implements OnInit {
  visit: Visit;
  resident: Resident;

  typesOfVisit: ItemSelect[] = [
    { value: 'Reunion', viewValue: 'Reunion' },
    { value: 'Amigo o Familiar', viewValue: 'Amigo o Familiar' },
    { value: 'Entregas', viewValue: 'Entregas' }
  ];

  constructor(private modalController: ModalController, private _visitsServices: VisitsService,
    private _toastServices: ToastService, private storage: Storage) {
    this.initialize();
  }

  initialize() {
    this.visit = new Visit();
    this.visit.dateIn = new Date().toISOString();
  }

  ngOnInit() {
    this.storage.get('resident').then(data => {
      this.resident = data;
    });
  }

  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  createVisit() {
    this.visit.residentId = this.resident.id;
    this.visit.visitingLocationId = this.resident.homeId;
    this.visit.status = 'ACTIVO';
    this._visitsServices.create(this.visit)
      .subscribe(res => {
        this._toastServices.create('Registrar Visita', 'Datos guardados satisfactoriamente...', "success");
        this.dismiss();
      });
  }

  cancel() {
    this.dismiss();
  }
}
