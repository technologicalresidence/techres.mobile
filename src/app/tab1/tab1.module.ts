import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatInputModule, MatOptionModule, MatSelectModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { IonicModule } from '@ionic/angular';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { CreateQrVisitComponent } from './create-qr-visit/create-qr-visit.component';
import { CreateVisitComponent } from './create-visit/create-visit.component';
import { Tab1Page } from './tab1.page';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    MatInputModule,
    MatSelectModule,
    MatOptionModule,
    NgxQRCodeModule,
    RouterModule.forChild([{ path: '', component: Tab1Page }])
  ],
  declarations: [Tab1Page, CreateVisitComponent, CreateQrVisitComponent],
  entryComponents: [CreateVisitComponent, CreateQrVisitComponent],
  providers: [
    SocialSharing,
    Base64ToGallery,
    AndroidPermissions
  ]
})
export class Tab1PageModule { }
