import { Component, Input, OnInit } from '@angular/core';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ModalController } from '@ionic/angular';
import { Visit } from '../../models';

@Component({
  selector: 'app-create-qr-visit',
  templateUrl: './create-qr-visit.component.html',
  styleUrls: ['./create-qr-visit.component.scss']
})
export class CreateQrVisitComponent implements OnInit {
  @Input('visit') visit: Visit;

  qrData: string;

  constructor(private modalController: ModalController,
    private base64ToGallery: Base64ToGallery,
    private socialSharing: SocialSharing,
    private androidPermissions: AndroidPermissions) {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
      result => console.log("Permissions granted", result.hasPermission),
      error => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
    );
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE]);
  }

  ngOnInit() {
    const visitJsonString = JSON.stringify(this.visit);
    this.qrData = visitJsonString;
  }

  share() {
    const canvas = document.querySelector('canvas') as HTMLCanvasElement;
    const imageData = canvas.toDataURL('image/jpeg').toString();
    let data = imageData.split(',')[1];

    this.base64ToGallery.base64ToGallery(data, { prefix: '_img', mediaScanner: true }).then(
      filePathInDevice => {
        this.socialSharing.share('Codigo de seguridad QR que necesitara para poder entrar.', 'Invitacion', `file://${filePathInDevice}`, null).then((res) => {
          console.log(res);
          this.cancel();
        });
      }
    );
  }

  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }

  cancel() {
    this.dismiss();
  }
}
