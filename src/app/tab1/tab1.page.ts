import { Component, OnInit } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ModalController } from '@ionic/angular';
import { Visit } from '../models/index';
import { ToastService, VisitsService } from '../services/index';
import { CreateQrVisitComponent } from './create-qr-visit/create-qr-visit.component';
import { CreateVisitComponent } from './create-visit/create-visit.component';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  visits: Visit[];


  constructor(private _visitsServices: VisitsService,
    private modalController: ModalController,
    private _toastServices: ToastService,
    private socialSharing: SocialSharing) {
    this.initialize();
  }

  initialize() {
    this.visits = new Array<Visit>();
  }

  ngOnInit() {
    this.getVisits('ACTIVO');
  }


  presentModal() {
    this.modalController.create({
      component: CreateVisitComponent
    }).then(modal => {
      modal.present();
      modal.onDidDismiss().then(data => {
        this.getVisits('ACTIVO');
      });
    });
  }

  share(visit: Visit) {
    this.modalController.create({
      component: CreateQrVisitComponent,
      componentProps: {
        visit: visit
      }
    }).then(modal => {
      modal.present();
      modal.onDidDismiss().then(() => { });
    });
  }

  getVisits(state: string) {
    this._visitsServices.getFilterByStatus("ACTIVO")
      .subscribe((res: Visit[]) => {
        this.visits = res;
      })
  }

  deleteVisit(id: number) {
    this._visitsServices.delete(id)
      .subscribe(() => {
        this._toastServices.create('Visitas', 'Dato eliminado satisfactoriamente...', 'success');
        this.getVisits('ACTIVO')
      })
  }
}
