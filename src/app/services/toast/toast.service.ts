import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(public toastController: ToastController) { }

  create(header: string, message: string, color: string = 'warning') {
    this.toastController.create({
      header: header,
      message: message,
      color: color,
      position: 'top',
      duration: 2000
    }).then(toastData => toastData.present());
  }
}
