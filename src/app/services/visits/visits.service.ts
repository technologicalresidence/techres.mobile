import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Visit } from '../../models/index';

@Injectable({
  providedIn: 'root'
})
export class VisitsService {
  api: string = environment.restApiBaseAddress + '/visits';

  constructor(public http: HttpClient) { }

  create(visit: Visit) {
    return this.http.post(this.api, visit);
  }

  delete(id: number) {
    const url = `${this.api}/${id}`;
    return this.http.delete(url);
  }

  getFilterByStatus(status: string) {
    const url = `${this.api}?status=${status}`;
    return this.http.get(url);
  }
}
