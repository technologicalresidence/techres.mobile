export class Resident {
    public id: number;
    public name: string;
    public phone: string;
    public homeId: number;
}