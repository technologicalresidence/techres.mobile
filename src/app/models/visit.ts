
export class Visit {
    public id: number;
    public residentId: number;
    public visitingLocationId: number;
    public visitorName: string;
    public dateIn: string;
    public dateOut: string;
    public idNumber: string;
    public typeOfVisit: string;
    public notes: string;
    public status: string;
}